﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace barley_break
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        GameLogic game;

        // Количество строк и стобцов игрового поля.
        // поумолчанию игровое поле 4x4
        int countRowAndCol = 4;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void gameStart(int size)
        {
            game = new GameLogic(size);
            game.start();
            for (int i = 0; i < 1000; i++)
            {
                game.moveShuffle();
            }
        }

        //  Глобальные переменные
        // Игровой интерфейс служит для общего доступа
        Grid gameInterface = new Grid();
        Grid playableArea = new Grid();

        // Создание списков хранения объектов "строк" и "столбцов"
        Button [,] buttons;

        // Вызов игрового поля и интерфейса
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sizeOfPlayableZone.Text.Length == 0 || sizeOfPlayableZone.Text == "1")
            {
                return;
            }
            else
            {
                Grid gameInterface = new Grid();
                gameStart(countRowAndCol);
                MainGrid.Children.Clear();
                gameInterface.Children.Clear();

                // Создание грида для кнопок взимодействия с интерфейсом
                Grid controlGameInterface = new Grid();
                ColumnDefinition controlCol1 = new ColumnDefinition();
                RowDefinition controlRow1 = new RowDefinition();
                RowDefinition controlRow2 = new RowDefinition();
                RowDefinition controlRow3 = new RowDefinition();
                controlRow1.Height = new GridLength(5, GridUnitType.Star);
                controlRow2.Height = new GridLength(5, GridUnitType.Star);
                controlRow3.Height = new GridLength(2, GridUnitType.Star);
                Grid.SetColumn(controlGameInterface, 1);
                Grid.SetRow(controlGameInterface, 1);
                controlGameInterface.ColumnDefinitions.Add(controlCol1);
                controlGameInterface.RowDefinitions.Add(controlRow1);
                controlGameInterface.RowDefinitions.Add(controlRow2);

                // Create a button.
                Button exitButton = new Button();
                Button shuffleButton = new Button();
                exitButton.Click += new RoutedEventHandler(exitButton_Click);
                shuffleButton.Click += new RoutedEventHandler(shuffleBars_Click);
                // Set properties.
                exitButton.Content = "Выход";
                shuffleButton.Content = "Перемешать";

                // Задаем игровой интерфейс который хранит игровое поле и элементы управления

                // Создание колонок в гриде интерфейса
                ColumnDefinition GridCol1 = new ColumnDefinition();
                ColumnDefinition GridCol2 = new ColumnDefinition();
                // Задание свойств колонкам интерфейса
                GridCol1.Width = new GridLength(2, GridUnitType.Star);
                GridCol2.Width = new GridLength(1, GridUnitType.Star);

                // Создание строк в гриде интерфейса
                RowDefinition GridRow1 = new RowDefinition();
                // Задание свойств строкам интерфейса
                GridRow1.Height = new GridLength(2, GridUnitType.Star);


                // Добавление колонок и строк в грид интерфейса
                gameInterface.ColumnDefinitions.Add(GridCol1);
                gameInterface.ColumnDefinitions.Add(GridCol2);

                gameInterface.RowDefinitions.Add(GridRow1);

                // Задает позицию объекта куда он будет добавлен
                Grid.SetRow(shuffleButton, 0);
                Grid.SetColumn(shuffleButton, 0);
                Grid.SetRow(exitButton, 1);
                Grid.SetColumn(exitButton, 0);

                // Добавление объекта в грид с указанной пизицей
                controlGameInterface.Children.Add(shuffleButton);
                controlGameInterface.Children.Add(exitButton);

                // Clear mainGrid
                MainGrid.Children.Clear();

                // Add created button to a previously created container.
                MainGrid.Children.Add(gameInterface);

                // Создание игровой зоны
                createPlayableArea(countRowAndCol);

                // Добавление игрового поля в окно приложения и элементов управления
                gameInterface.Children.Add(playableArea);
                gameInterface.Children.Add(controlGameInterface);

                // Создание в игровой зоне игровых элементов (плиток)
                createBars(countRowAndCol);

                refresh();
            }
            
        }

        // Создает игровую зону
        private void createPlayableArea(int rowAndCol)
        {
            playableArea = new Grid();
            int countRow = rowAndCol;
            int countCol = rowAndCol;
            playableArea.Children.Clear();
            playableArea.ShowGridLines = true;

            // Создание списков хранения объектов "строк" и "столбцов"
            List<RowDefinition> rows = new List<RowDefinition>();
            List<ColumnDefinition> cols = new List<ColumnDefinition>();
            rows.Clear();
            cols.Clear();

            // Инициалзация строк
            for (int i = 0; i < countRow; i++)
            {
                RowDefinition row = new RowDefinition();
                // Задание свойств строке
                row.Height = new GridLength(1, GridUnitType.Star);
                rows.Add(row);
            }
            // Инициализация столбцов
            for (int i = 0; i < countCol; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                // Задание свойств столбцу
                col.Width = new GridLength(1, GridUnitType.Star);
                cols.Add(col);
            }

            // Прорисовка строк
            foreach (RowDefinition row in rows)
            {
                playableArea.RowDefinitions.Add(row);
            }
            // Прорисовка столбцов
            foreach (ColumnDefinition col in cols)
            {
                playableArea.ColumnDefinitions.Add(col);
            }

            // Задает позицию объекта куда он будет добавлен
            Grid.SetRow(playableArea, 0);
            Grid.SetColumn(playableArea, 0);
        }


        // Служит для создания игровых плиток
        private void createBars(int rowAndCol)
        {
            int countRow = rowAndCol;
            int countCol = rowAndCol;
            int barCounter = 1;
            buttons = new Button[countRow, countCol];
            for (int i = 0; i < countRow; i++)
            {
                for (int j = 0; j < countCol; j++)
                {
                    Button bar = new Button();
                    buttons[i, j] = bar;
                    bar.Content = barCounter++;
                    bar.Background = Brushes.DimGray;
                    bar.Click += new RoutedEventHandler(Bar_Click);

                    // Если последняя ячейка перебора, то не добавляем Элемент
                    if (i == countRow - 1 && j == countCol - 1)
                    {
                        bar.Content = 0;
                        bar.Visibility = Visibility.Hidden;
                    }
                    Grid.SetRow(bar, i);
                    Grid.SetColumn(bar, j);
                    playableArea.Children.Add(bar);
                }
            }
        }

        // Служит для возврата в главное меню
        private void exitButton_Click(object sender, EventArgs e)
        {
            createMainMenu();
        }

        // Создание главного меню
        public void createMainMenu()
        {
            MainGrid.Children.Clear();
            playableArea.Children.Clear();
            gameInterface.Children.Clear();

            // Создание свойств для кнопки "играть" главного меню
            Button btnPlay = new Button();
            btnPlay.Name = "btnPlay";
            btnPlay.Content = "Играть";
            btnPlay.Margin = new Thickness(230, 155, 0, 0);
            btnPlay.VerticalAlignment = VerticalAlignment.Top;
            btnPlay.HorizontalAlignment = HorizontalAlignment.Left;
            btnPlay.Width = 360;
            btnPlay.Height = 100;
            btnPlay.FontSize = 36;
            btnPlay.Click += new RoutedEventHandler(Button_Click);

            // Создание свойств для кнопки текстового поля главного меню
            //TextBox sizeOfPlayableZone = new TextBox();
            sizeOfPlayableZone.Name = "sizeOfPlayableZone";
            sizeOfPlayableZone.Text = countRowAndCol.ToString();
            sizeOfPlayableZone.TextWrapping = TextWrapping.Wrap;
            sizeOfPlayableZone.Margin = new Thickness(340, 0, 315, 40);
            sizeOfPlayableZone.VerticalAlignment = VerticalAlignment.Bottom;
            sizeOfPlayableZone.HorizontalAlignment = HorizontalAlignment.Center;
            sizeOfPlayableZone.Width = 140;
            sizeOfPlayableZone.Height = 80;
            sizeOfPlayableZone.FontSize = 48;
            sizeOfPlayableZone.HorizontalContentAlignment = HorizontalAlignment.Center;
            sizeOfPlayableZone.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            //sizeOfPlayableZone.TextChanged += new TextChangedEventHandler(SizeOfPlayableZone_TextChanged);

            // Создание надписи приветствия
            Label greetingLbl = new Label();
            greetingLbl.Name = "label";
            greetingLbl.Content = "Добро пожаловать в пятнашки!";
            greetingLbl.HorizontalAlignment = HorizontalAlignment.Left;
            greetingLbl.Margin = new Thickness(155, 64, 0, 0);
            greetingLbl.VerticalAlignment = VerticalAlignment.Top;
            greetingLbl.FontSize = 36;

            // Прорисовка кнопок в окне
            MainGrid.Children.Add(btnPlay);
            MainGrid.Children.Add(sizeOfPlayableZone);
            MainGrid.Children.Add(greetingLbl);
        }

        // Вызывает передвижение плитки
        private void Bar_Click(object sender, RoutedEventArgs e)
        {
            int position = (Int32)((Button)sender).Content;
            game.moveBar(position);
            refresh();
            // Вызывает сообщение о победе
            if (game.checkVictory())
            {
                MessageBox.Show("Ура! Вы победили!");
            }
        }

        // Обновляет позиции кнопок в интерфейсе
        public void refresh()
        {
            for (int positionX = 0; positionX < game.getSize(); positionX++)
            {
                for (int positionY = 0; positionY < game.getSize(); positionY++)
                {
                    int numberOfBtn = game.getNumber(positionX, positionY);
                    buttons[positionX, positionY].Content = numberOfBtn;
                    
                    if (numberOfBtn > 0)
                    {
                        buttons[positionX, positionY].Visibility = Visibility.Visible;
                    }
                    else
                    {
                        buttons[positionX, positionY].Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        // Служит для перемешивание плиток в игровом поле
        public void shuffleBars_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 1000; i++)
            {
                game.moveShuffle();
            }
            refresh();
        }


        // Меняем размеры игрового поля если они менялись
        private void SizeOfPlayableZone_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                countRowAndCol = Int32.Parse(sizeOfPlayableZone.Text);
            }
            catch (Exception)
            {
            }
        }

        private void SizeOfPlayableZone_KeyDown(object sender, KeyEventArgs e)
        {
            if (((TextBox)sender).Text.Length == 0)
            {
                if (e.Key == Key.D0) e.Handled = true;
                else if (e.Key < Key.D1
 || e.Key > Key.D9)
                {
                    e.Handled = true;
                }
            }
            else if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                e.Handled = true;
            }
        }
    }
}
