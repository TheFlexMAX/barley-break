﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace barley_break
{
    class GameLogic
    {
        int size; // Задает размер игрового поля
        int[,] gameArea;
        static Random rand = new Random();
        int emptyPosX;
        int emptyPosY;

        public GameLogic (int size)
        {
            this.size = size;
            this.gameArea = new int[size, size];
        }


        // Метод инициализации игрового поля и пустого элемента
        public void start()
        {
            int count = 1;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    gameArea[x, y] = count++;
                }
            }
            emptyPosX = size - 1;
            emptyPosY = size - 1;
            gameArea[emptyPosX,emptyPosY] = 0;
        }

        public int getSize()
        {
            return this.size;
        }

        public int[,] getGameArea()
        {
            return this.gameArea;
        }

        // Служит для получения числа по координатам
        public int getNumber(int transferX, int transferY)
        {
            if (transferX < 0 || transferX >= size)
            {
                return 0;
            }
            if (transferY < 0 || transferY >= size)
            {
                return 0;
            }
            return gameArea[transferX, transferY];
        }


        // Возвращает координаты плитки по переданному числу
        public void NumberToCoords(int number, out int x, out int y)
        {
            x = 0;
            y = 0;
            if (number < 0)
            {
                number = 0;
            }
            if (number > size * size - 1)
            {
                number = size * size - 1;
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (gameArea[i, j] == number)
                    {
                        x = i;
                        y = j;
                        return;
                    }
                }
            }
        }

        // Возвращает число из игрового поля по заданным координатам
        public int CoordsToNumber(int x, int y)
        {
            if (x < 0) x = 0;
            if (x > size - 1) x = size - 1;
            if (y < 0) y = 0;
            if (y > size - 1) y = size - 1;
            return gameArea[x, y];
        }

        // Метод передвижение двумерного массива.
        // в качестве передаваемого числа передается номер плитки
        public void moveBar(int number)
        {
            // Узнаем координаты нажатой плитки
            int x, y;
            NumberToCoords(number, out x, out y);

            if (Math.Abs(emptyPosX - x) + Math.Abs(emptyPosY - y) != 1)
            {
                return;
            }

            gameArea[emptyPosX, emptyPosY] = gameArea[x, y];
            gameArea[x, y] = 0;
            emptyPosX = x;
            emptyPosY = y;
        }


        // Перемешивание плиток
        public void moveShuffle()
        {
            // moveBar(rand.Next(0, size * size));
            int a = rand.Next(0, 4);
            int x = emptyPosX;
            int y = emptyPosY;
            switch (a)
            {
                case 0: x--; break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
            }
            moveBar(CoordsToNumber(x, y));
        }

        // Метод проверки победы 
        public bool checkVictory()
        {
            bool win = true;
            // Если ноль в правом нижнем углу, то проверяем на правильность сборки
            if (gameArea[size - 1, size - 1] != 0)
            {
                return false;
            }

            int count = 1;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    if (x == size - 1 && y == size - 1) continue;
                    if (gameArea[x, y] != count)
                    {
                        return win = false;
                    }
                    count++;
                }
            }

            return win;
        }
    }
}
